"""
A basic implementation of the gsutil command cp -r for copying from a Google Cloud
Storage URI to a local file system path

Example from gsutil `gsutil cp -r gs://online-infra-engineer-test/mydir /some/path`

Usage:
  fake-gsutil-cp.py [-m] SOURCE DESTINATION

Arguments:
  SOURCE         Source for the copy, should be a google cloud storage
  DESTINATION    Destination

Options:
  -m             Use multiprocessing
  -h --help      Show this screen.
  --version      Show version.
"""

import os
import re
import sys
from docopt import docopt
from google.cloud import storage
from multiprocessing import Pool


GS_PATH_REGEX = r"gs:\/\/([a-z-_\.]*)\/(.*)"


def validate_arguments():
    arguments = docopt(__doc__, version='Fake GSUtil cp -r 0.1')

    path_regex = re.compile(GS_PATH_REGEX)

    validated_arguments = {
        'multithread': arguments['-m']
    }

    source_match = path_regex.match(arguments['SOURCE'])
    if source_match is not None:
        validated_arguments['source'] = {
            'bucket': source_match.group(1),
            'path': source_match.group(2)
        }
    else:
        print("Not a valid source path")
        sys.exit(2)

    destination_match = path_regex.match(arguments['DESTINATION'])
    if destination_match is not None:
        print("Tool does not support uploads to GS or copying to GS buckets")
        sys.exit(2)
    else:
        validated_arguments['destination'] = arguments['DESTINATION']

    return validated_arguments


def download_path(args):
    storage_client = storage.Client()
    bucket = storage_client.bucket(args['source']['bucket'])
    blob_list = bucket.list_blobs(prefix=args['source']['path'])

    local_download_path = os.path.join(args['destination'])
    os.makedirs(local_download_path, exist_ok=True)

    blobs = [{
        'dst': local_download_path,
        'uri': "gs://%s/%s" % (blob.bucket.name, blob.name),
        'name': blob.name}
             for blob in blob_list]

    if args['multithread']:
        with Pool() as pool:
            pool.map(download_blob, blobs)
    else:
        for blob in blobs:
            download_blob(blob)


def download_blob(blob):
    # This is horrible, but client is not pickleable so can't be
    # passed directly to pool.map, also using a global client
    # creates SSL problems.
    client = storage.Client()
    dst_path = os.path.join(blob['dst'], blob['name'])
    os.makedirs(os.path.dirname(dst_path), exist_ok=True)
    with open(dst_path, 'wb') as file_to_write:
        client.download_blob_to_file(blob['uri'], file_to_write)


def main():
    args = validate_arguments()

    download_path(args)


if __name__ == "__main__":
    main()
