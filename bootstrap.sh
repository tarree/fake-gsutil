
if [ ! -d "venv" ]
then
   echo "Creating virtual env"
   python -m venv venv
fi

echo "Activating virtualenv"

. venv/bin/activate

pip install -r requirements.txt
